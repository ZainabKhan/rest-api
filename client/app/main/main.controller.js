'use strict';

angular.module('restApiApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.awesomeThings = [];

    $http.get('/api/projects').success(function(awesomeProjects) {
      $scope.awesomeProjects = awesomeProjects;
    });

    $scope.addProject = function() {
      if($scope.newProject === '') {
        return;
      }
      $http.post('/api/projects', { name: $scope.newProject });
      $scope.newProject = '';
    };

    $scope.deleteProject = function(project) {
      $http.delete('/api/projects/' + project._id);
    };
  });

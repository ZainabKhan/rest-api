'use strict';

var should = require('should');
var app = require('../../app');
var Issue = require('./issue.model');

var issue = new Issue({
  title: 'test issue',
  description: 'this is a test issue',
  project: 'asdasdsadq2e21wd',
  assignee: 'kcjasd6fisbc7d',
  creator: 'kcjasd6fisbc7d',
  state: 'open'
});

describe('Issue Model', function() {
    before(function(done) {
    // Clear issues before testing
    Issue.remove().exec().then(function() {
      done();
    });
  });

  afterEach(function(done) {
    Issue.remove().exec().then(function() {
      done();
    });
  });

  it('should begin with no issues', function(done) {
    Issue.find({}, function(err, issues) {
      issues.should.have.length(0);
      done();
    });
  });

  it('should fail when saving without a title', function(done) {
    issue.title = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a description', function(done) {
    issue.description = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without created at date', function(done) {
    issue.createdAt = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without updated at date', function(done) {
    issue.updatedAt = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a creator', function(done) {
    issue.creator = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a project', function(done) {
    issue.project = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a state', function(done) {
    issue.state = '';
    issue.save(function(err) {
      should.exist(err);
      done();
    });
  });

});

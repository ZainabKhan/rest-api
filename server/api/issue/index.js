'use strict';

var express = require('express');
var controller = require('./issue.controller');
var commentController = require('../comment/comment.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/:id/comments', auth.isAuthenticated(), commentController.index);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.post('/:id/comments', auth.isAuthenticated(), commentController.create);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
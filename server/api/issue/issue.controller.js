'use strict';

var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var Issue = require('./issue.model');
var commentController = require('../comment/comment.controller');
var _ = require('lodash');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

// Get list of issues of project
exports.index = function(req, res) {
    Issue.find({
        project: req.params.id
    }, function(err, issue) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(issue);
    });
};

// Get a single issue
exports.show = function(req, res) {
    Issue.findById(req.params.id, function(err, issue) {
        if (err) {
            return handleError(res, err);
        }
        if (!issue) {
            return res.status(404).send('Not Found');
        }
        return res.json(issue);
    });
};

// Creates a new thing in the DB.
exports.create = function(req, res) {
    var newIssue = new Issue(req.body);
    newIssue.title = req.body.title;
    newIssue.description = req.body.description;
    newIssue.project = req.params.id;
    newIssue.assignee = req.body.assignee;
    newIssue.creator = req.user._id;
    newIssue.state = req.body.state;
    newIssue.createdAt = new Date();
    newIssue.updatedAt = new Date();
    newIssue.save(function(err, project) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(project);
    });
};

// Updates an existing issue in the DB.
exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Issue.findById(req.params.id, function(err, issue) {
        if (err) {
            return handleError(res, err);
        }
        if (!issue) {
            return res.status(404).send('Not Found');
        }
        var updated = _.merge(issue, req.body);
        updated.updatedAt = new Date();
        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(issue);
        });
    });
};

// Deletes an issue from the DB.
exports.destroy = function(req, res) {
    Issue.findById(req.params.id, function(err, issue) {
        if (err) {
            return handleError(res, err);
        }
        if (!issue) {
            return res.status(404).send('Not Found');
        }

        commentController.destroyIssueComments();
        issue.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};

//Deletes project's issues
exports.destroyProjectIssues = function(projectId) {
    Issue.remove({
        project: projectId
    }, function(err, issues) {
        if (err) {
            return console.log(err);
        }
    });
}

function handleError(res, err) {
    return res.status(500).send(err);
}
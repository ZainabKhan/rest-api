'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//helper functions
var formatDate = function(value){
    return (value.getMonth() + 1) + 
    "-" + value.getDate() +
    "-" + value.getFullYear() + 
    " " + value.getHours() +
    ":" + value.getMinutes() +
    ":" + value.getSeconds();
};

//model
var IssueSchema = new Schema({
  title: String,
  description: String,
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  },
  assignee: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  updatedAt: {
    type: Date,
    default: new Date()
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  state: String
});

//public view
IssueSchema
  .virtual('profile')
  .get(function() {
    return {
      'title': this.title,
      'description': this.description,
      'project': this.project,
      'assignee': this.assignee,
      'createdAt': formatDate(this.createdAt),
      'updatedAt': formatDate(this.updatedAt),
      'creator': this.creator,
      'state': this.state
    };
  });

//info with token
IssueSchema
  .virtual('token')
  .get(function() {
    return {
      '_id': this._id,
      'title': this.title,
      'createdAt': this.createdAt,
      'updatedAt': this.updatedAt
    };
  });

//validations
// valiadte empty title
IssueSchema
  .path('title')
  .validate(function(title) {
    return title.length;
  }, 'Title cannot be blank, issue must have a title.');

// valiadte empty description
IssueSchema
  .path('description')
  .validate(function(owner) {
    return owner.length;
  }, 'description cannot be blank, issue must have a description.');

// valiadte empty project
IssueSchema
  .path('project')
  .validate(function(owner) {
    return owner.length;
  }, 'project cannot be blank, issue must have in a project.');

// valiadte empty creator
IssueSchema
  .path('creator')
  .validate(function(owner) {
    return owner.length;
  }, 'creator cannot be blank, issue must have a creator.');

// valiadte empty state
IssueSchema
  .path('state')
  .validate(function(owner) {
    return owner.length;
  }, 'state cannot be blank, issue must have a state.');

// valiadte empty created at date
IssueSchema
.path('createdAt')
.validate(function(createdAt) {
  return createdAt.length;
}, 'createdAt cannot be blank');

// valiadte empty updated at date
IssueSchema
.path('updatedAt')
.validate(function(updatedAt) {
  return updatedAt.length;
}, 'updatedAt cannot be blank');

//pre-save
IssueSchema
  .pre('save', function(next) {
    if (!this.isNew){
      return next();
    } else {
		  next();
    }
  });
//methods

module.exports = mongoose.model('Issue', IssueSchema);
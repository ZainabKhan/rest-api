'use strict';

var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var Comment = require('./comment.model');
var _ = require('lodash');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

// Get list of comments on issue
exports.index = function(req, res) {
    Comment.find({
        commentedOn: req.params.id
    }, function(err, comment) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(comment);
    });
};

// Get a single comment
exports.show = function(req, res) {
    Comment.findById(req.params.id, function(err, comment) {
        if (err) {
            return handleError(res, err);
        }
        if (!comment) {
            return res.status(404).send('Not Found');
        }
        return res.json(comment);
    });
};

// Creates a new comment.
exports.create = function(req, res) {
    var newComment = new Comment(req.body);
    newComment.content = req.body.content;
    newComment.commentedOn = req.params.id;
    newComment.postedBy = req.user._id;
    newComment.createdAt = new Date();
    newComment.updatedAt = new Date();
    newComment.save(function(err, comment) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(comment);
    });
};

// Updates an existing comment in the DB.
exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Comment.findById(req.params.id, function(err, comment) {
        if (err) {
            return handleError(res, err);
        }
        if (!comment) {
            return res.status(404).send('Not Found');
        }
        var updated = _.merge(comment, req.body);
        updated.updatedAt = new Date();
        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(comment);
        });
    });
};

// Deletes an comment from the DB.
exports.destroy = function(req, res) {
    Comment.findById(req.params.id, function(err, comment) {
        if (err) {
            return handleError(res, err);
        }
        if (!comment) {
            return res.status(404).send('Not Found');
        }
        comment.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};

//Deletes issue's comments
exports.destroyProjectIssues = function(issueId) {
    Comment.remove({
        commentedOn: issueId
    }, function(err, comments) {
        if (err) {
            return console.log(err);
        }
    });
}

function handleError(res, err) {
    return res.status(500).send(err);
}
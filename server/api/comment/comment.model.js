'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//helper functions
var formatDate = function(value){
    return (value.getMonth() + 1) + 
    "-" + value.getDate() +
    "-" + value.getFullYear() + 
    " " + value.getHours() +
    ":" + value.getMinutes() +
    ":" + value.getSeconds();
};

//model
var CommentSchema = new Schema({
  content: String,
  commentedOn: {
    type: Schema.Types.ObjectId,
    ref: 'Issue'
  },
  postedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  updatedAt: {
    type: Date,
    default: new Date()
  }
});

//public view
CommentSchema
  .virtual('profile')
  .get(function() {
    return {
      'content': this.content,
      'postedBy': this.postedBy,
      'createdAt': formatDate(this.createdAt),
      'updatedAt': formatDate(this.updatedAt)
    };
  });

//info with token
CommentSchema
  .virtual('token')
  .get(function() {
    return {
      '_id': this._id,
      'createdAt': this.createdAt,
      'updatedAt': this.updatedAt
    };
  });

//validations
// valiadte empty content
CommentSchema
  .path('content')
  .validate(function(content) {
    return content.length;
  }, 'Content cannot be blank, comment must have some content.');

// valiadte empty issue field
CommentSchema
  .path('commentedOn')
  .validate(function(commentedOn) {
    return commentedOn.length;
  }, 'Issue cannot be blank, comment must be posted on some issue.');

// valiadte empty creator
CommentSchema
  .path('postedBy')
  .validate(function(postedBy) {
    return postedBy.length;
  }, 'creator cannot be blank, comment must have a creator.');

// valiadte empty created at date
CommentSchema
.path('createdAt')
.validate(function(createdAt) {
  return createdAt.length;
}, 'createdAt cannot be blank');

// valiadte empty updated at date
CommentSchema
.path('updatedAt')
.validate(function(updatedAt) {
  return updatedAt.length;
}, 'updatedAt cannot be blank');

//pre-save
CommentSchema
  .pre('save', function(next) {
    if (!this.isNew){
      return next();
    } else {
		  next();
    }
  });
//methods

module.exports = mongoose.model('Comment', CommentSchema);
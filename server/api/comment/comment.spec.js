'use strict';

var should = require('should');
var app = require('../../app');
var Comment = require('./comment.model');

var comment = new Comment({
  content: 'Finallly a comment.',
  commentedOn: 'lajgdf87agfkae',
  postedBy: 'kcjasd6fisbc7d'
});

describe('Comment Model', function() {
    before(function(done) {
    // Clear comments before testing
    Comment.remove().exec().then(function() {
      done();
    });
  });

  afterEach(function(done) {
    Comment.remove().exec().then(function() {
      done();
    });
  });

  it('should begin with no comments', function(done) {
    Comment.find({}, function(err, comments) {
      comments.should.have.length(0);
      done();
    });
  });

  it('should fail when saving without a content', function(done) {
    comment.content = '';
    comment.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without issue refrence', function(done) {
    comment.commentedOn = '';
    comment.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without created at date', function(done) {
    comment.createdAt = '';
    comment.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without updated at date', function(done) {
    comment.updatedAt = '';
    comment.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a creator', function(done) {
    comment.postedBy = '';
    comment.save(function(err) {
      should.exist(err);
      done();
    });
  });

});

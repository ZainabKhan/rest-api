'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//helper functions
var formatDate = function(value){
    return (value.getMonth() + 1) + 
    "-" + value.getDate() +
    "-" + value.getFullYear() + 
    " " + value.getHours() +
    ":" + value.getMinutes() +
    ":" + value.getSeconds();
};

//model
var ProjectSchema = new Schema({
  title: String,
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  users: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  createdAt: {
    type: Date,
    default: new Date()
  },
  updatedAt: {
    type: Date,
    default: new Date()
  }
});

//public view
ProjectSchema
  .virtual('profile')
  .get(function() {
    return {
      'title': this.title,
      'owner': this.owner,
      'users': this.users,
      'createdAt': formatDate(this.createdAt),
      'updatedAt': formatDate(this.updatedAt)
    };
  });

//info with token
ProjectSchema
  .virtual('token')
  .get(function() {
    return {
      '_id': this._id,
      'title': this.title,
      'createdAt': this.createdAt,
      'updatedAt': this.updatedAt
    };
  });

//validations
// valiadte empty title
ProjectSchema
  .path('title')
  .validate(function(title) {
    return title.length;
  }, 'Title cannot be blank, project must have title.');

// valiadte empty owner
ProjectSchema
  .path('owner')
  .validate(function(owner) {
    return owner.length;
  }, 'Owner cannot be blank, project must have an owner.');

// valiadte empty created at date
ProjectSchema
.path('createdAt')
.validate(function(createdAt) {
  return createdAt.length;
}, 'createdAt cannot be blank');

// valiadte empty updated at date
ProjectSchema
.path('updatedAt')
.validate(function(updatedAt) {
  return updatedAt.length;
}, 'updatedAt cannot be blank');

//pre-save
ProjectSchema
  .pre('save', function(next) {
    if (!this.isNew){
      return next();
    } else {
		  next();
    }
  });
//methods

module.exports = mongoose.model('Project', ProjectSchema);
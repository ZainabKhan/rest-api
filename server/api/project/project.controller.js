'use strict';

var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var Project = require('./project.model');
var issueController = require('../issue/issue.controller');
var _ = require('lodash');

var validationError = function(res, err) {
  return res.status(422).json(err);
};

// Get list of projects
exports.index = function(req, res) {
  Project.find(function (err, projects) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(projects);
  });
};

// Get a single project
exports.show = function(req, res) {
  Project.findById(req.params.id, function (err, project) {
    if(err) { return handleError(res, err); }
    if(!project) { return res.status(404).send('Not Found'); }
    return res.json(project);
  });
};

//Get user projects
exports.getUserProjects = function(req, res) {
  Project.find({owner: req.params.id} , function (err, project) {
    if (err) {
      return handleError(res, err);
    }

    if(!project) { return res.status(404).send('Not Found'); }
    return res.json(project);
  });
};

// Creates a new thing in the DB.
exports.create = function(req, res) {
  var newProject = new Project(req.body);
  newProject.title = req.body.title;
  newProject.owner = req.user._id;
  newProject.users = req.body.users;
  newProject.createdAt = new Date();
  newProject.updatedAt = new Date();
  newProject.save(function(err, project) {
    if (err) { return handleError(res, err); }
      return res.status(200).json(project);
  });
};

// Updates an existing project in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Project.findById(req.params.id, function (err, project) {
    if (err) { return handleError(res, err); }
    if(!project) { return res.status(404).send('Not Found'); }
    var updated = _.merge(project, req.body);
    updated.updatedAt = new Date();
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(project);
    });
  });
};

// Deletes a project from the DB.
exports.destroy = function(req, res) {
  Project.findById(req.params.id, function (err, project) {
    if(err) { return handleError(res, err); }
    if(!project) { return res.status(404).send('Not Found'); }
    issueController.destroyProjectIssues(req.params.id);
    project.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
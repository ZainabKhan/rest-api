'use strict';

var should = require('should');
var app = require('../../app');
var Project = require('./project.model');

var project = new Project({
  title: 'test project',
  owner: 'Fake User',
  users: ['dasdas','fasfs']
});

describe('Project Model', function() {
    before(function(done) {
    // Clear projects before testing
    Project.remove().exec().then(function() {
      done();
    });
  });

  afterEach(function(done) {
    Project.remove().exec().then(function() {
      done();
    });
  });

  it('should begin with no projects', function(done) {
    Project.find({}, function(err, projects) {
      projects.should.have.length(0);
      done();
    });
  });

  it('should fail when saving without a title', function(done) {
    project.title = '';
    project.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without an owner', function(done) {
    project.owner = '';
    project.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without created at date', function(done) {
    project.createdAt = '';
    project.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without updated at date', function(done) {
    project.updatedAt = '';
    project.save(function(err) {
      should.exist(err);
      done();
    });
  });

});

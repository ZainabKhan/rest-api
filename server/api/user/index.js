'use strict';

var express = require('express');
var controller = require('./user.controller');
var projectController = require('../project/project.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/:id/projects', auth.isAuthenticated(), projectController.getUserProjects);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.put('/:id/', auth.isAuthenticated(), controller.update);
router.post('/', controller.create);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;